"""
Test mongo handler
"""
import unittest, inspect
from db.mongodb.mongodb_handler import MongoDbHandler

class MongoDbHandlerTestCase(unittest.TestCase):
    """
    MongodbHanlderTestCase
    """
    def setUp(self):
        self.mongodb = MongoDbHandler("local", "stocker_test", "stocks")

    def test_set_db(self):
        """
        test set_db
        """
        print(inspect.stack()[0][3])
        self.mongodb.set_db("stocker_test")
        self.assertEqual(self.mongodb.get_current_db_name(), "stocker_test")

    def test_set_collection(self):
        """
        test set_connections
        """
        print(inspect.stack()[0][3])
        self.mongodb.set_collection("stocks")
        self.assertEqual(self.mongodb.get_current_collection_name(), "stocks")

    def test_get_db_name(self):
        print(inspect.stack()[0][3])
        dbname = self.mongodb.get_current_db_name()
        assert dbname
        print(dbname)

    def test_get_collection_name(self):
        print(inspect.stack()[0][3])
        collection_name = self.mongodb.get_current_collection_name()
        assert collection_name
        print(collection_name)


    def test_insert_item(self):
        """
        test insert_item 
        """
        print(inspect.stack()[0][3])
        id = self.mongodb.insert_item({"code":"0003", "name":"TEST3"})
        print(id)

    def test_find_item(self):
        """
        test find_item
        """
        print(inspect.stack()[0][3])
        item = self.mongodb.find_item({"code":"0003"})
        if item is not None:
            pass
            #self.assertEqual(item["name"], "TEST3")
        print(item)

    def test_insert_items(self):
        """
        test insert_items
        """
        print(inspect.stack()[0][3])
        items = [{"code":"0004", "name":"Test4"},
                 {"code":"0005", "name":"Test5"}]
        ids = self.mongodb.insert_items(items)
        print(ids)

    def test_delete_items(self):
        print(inspect.stack()[0][3])
        items = [{"delete_code":"0004", "name":"Test4"},
                 {"delete_code":"0005", "name":"Test5"}]
        ids = self.mongodb.insert_items(items)
        print(ids)
        self.mongodb.delete_item({})

   
    def test_move_document(self):
        print(inspect.stack()[0][3])
        items = [{"delete_code":"0006", "name":"Test6"},
                 {"delete_code":"0007", "name":"Test7"}]
        ids = self.mongodb.insert_items(items)
        item_list = self.mongodb.find_item({})
        for item in item_list:
            self.mongodb.set_collection("stocks_test")
            self.mongodb.insert_item(item)
            self.mongodb.set_collection("stocks")
            self.mongodb.delete_item({"_id":item["_id"]})

    def test_aggregate(self):
        print(inspect.stack()[0][3])
        self.mongodb_remote = MongoDbHandler("remote", "coiner", "price_info")
        import datetime
        result = {}
        result_one = {}
        self.coin_type="bch_krw"
        for timedelta in [1,2,3,24]:
            now = datetime.datetime.now()
            delta = now - datetime.timedelta(minutes=timedelta*60)
            delta_timestamp = int(delta.timestamp())
            pipeline_amount = [{"$match":{"timestamp":{"$gt":delta_timestamp}, "coin":self.coin_type}},
                               {"$group":{"_id":"$coin",
                                          "min_val":{"$min":"$price"},
                                          "max_val":{"$max":"$price"},
                                          "sum_val":{"$sum":"$amount"}
                               }}]
            if timedelta == 1:
                result_one[timedelta] = [ item for item in self.mongodb_remote.aggregate(pipeline_amount)]
            else:
                result[timedelta] = [ item for item in self.mongodb_remote.aggregate(pipeline_amount)]

        for key in result.keys():
            print(str(key))
            print(result[key])
            for item in result[key]:
                print(item)
                max_val = int(item["max_val"])
                min_val = int(item["min_val"])
                sum_val = int(item["sum_val"])
                sum_avg_val = int(item["sum_val"])/int(key)

    def test_or_query(self):
        print(inspect.stack()[0][3])
        self.mongodb_remote = MongoDbHandler("remote", "trader", "trade_status")
        #results = self.mongodb_remote.find_item({"coin":"bch_krw", "$or":[{"status":"BUY_ORDERED"},{"status":"SELL_ORDERED"},{"status":"BUY_COMPLETED"}], "buy":{"$gt": "950000","$lte": "1000000"}},
        #                                        "trader","trade_status")
        results = self.mongodb_remote.find_item({"coin":"btc_krw", "$or":[{"status":"BUY_ORDERED"},{"status":"SELL_ORDERED"},{"status":"BUY_COMPLETED"}], "buy":{"$gt": "1000000"}},
                                                "trader","trade_status")
        for item in results:
            print(item) 

    def tearDown(self):
        pass
    
if __name__ == "__main__":
    unittest.main()
